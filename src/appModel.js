function isModeRunning(modeName) {
    let args = process.argv;
    const argsLength = args.length;
    for (let i = argsLength; i >= 2; i--) {
        if (args[i] === modeName) {
            return true;
        }
    }
    return false;
}

module.exports = {
    isModeRunning: isModeRunning
};
