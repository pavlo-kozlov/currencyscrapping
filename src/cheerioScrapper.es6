const request = require('request');
const cheerio = require('cheerio');

const url = 'http://minfin.com.ua/currency/auction/usd/sell/kiev/?presort=&sort=time&order=desc';

function loadPage() {
    console.log('loading page...');

    const options = {
        url: url,
        headers: {
            'User-Agent': 'request'
        }
    };

    request(options, function(err, resp, body) {
        console.log('page loaded');

        let $ = cheerio.load(body);
        let dealRow = $('.au-deals-list').find('.au-deal .au-deal-row');
        let currencyArray = dealRow.map(function() {
            let currency = $(this).find('.au-deal-currency').text();
            return currency ? {
                time: $(this).find('.au-deal-time').text(),
                currency: currency,
                amount: $(this).find('.au-deal-sum').text(),
                message: $(this).find('.au-msg-wrapper').text()
            } : null;
        }).get();
        console.log('result:');
        console.log(currencyArray);
    });
}

module.exports = {
    loadPage: loadPage
};
