const Nightmare = require('nightmare');

function loadPage(siteOptions) {
    const url = siteOptions.pageUrl;
    const selectors = siteOptions.selectors;
    return new Promise((resolve, reject) => {
        console.log('parsing', url);
        let nightmare = Nightmare({ show: false });
        nightmare
            .goto(url)
            .evaluate((selectors) => {
                function justText(node) {
                    return $(node).clone()
                        .children()
                        .remove()
                        .end()
                        .text()
                        .trim();
                }

                let result = [];

                for (let i = 0; i < selectors.length; i++) {
                    let selector = selectors[i];
                    let queryResult = document.querySelector(selector.query);
                    let resultObj = {
                        name: selector.name,
                        data: justText(queryResult)
                    };
                    if (selector.icon) {
                        let icon = document.querySelector(selector.icon);
                        if (icon) {
                            resultObj.icon = icon.src;
                        }
                    }
                    result.push(resultObj);
                }

                return result;
            }, selectors)
            .end()
            .then((result) => {
                resolve(result);
            })
            .catch((error) => {
                reject('Search failed:', error);
            });
    });
}

module.exports = {
    loadPage: loadPage
};
