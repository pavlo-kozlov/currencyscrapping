const fs = require('fs');
//const scrapper = require('./../build/cheerioScrapper');
const scrapper = require('./../build/nightmareScrapper');
const mailer = require('./../build/mailer');

const configs = require('../configs/selectors.json');
const emails = configs.emails.join(',');

console.log('Server started');

let gatheredData = [];
let sites = configs.sites;

let index = 0;
let loadSite = site => {
    return scrapper.loadPage(site).then(result => {
        console.log('site parsed');
        gatheredData.push(result);
    }).catch(err => {
        console.log('Error:', err);
    });
};

let loadNextSite = () => {
    if (index < sites.length) {
        loadSite(sites[index]).then(() => {
            index++;
            loadNextSite();
        }).catch(err => {
            console.log('why do I get this catch?', err);
        });
    } else {
        sendMail(gatheredData);
    }
};

let sendMail = (data) => {
    mailer.sendMail('Scrapping data', data, emails);
};

loadNextSite();
