const mailer = require('@nodemailer/pro');
const fs = require('fs');
const AppModel = require('./appModel');
const CONSTS = require('./consts');
const configs = require('../configs/app.config.json');

const STYLE_TR = 'border-top: 1px solid #C1C3D1; border-bottom-: 1px solid #C1C3D1; color: #666B85; font-size: 16px; font-weight: normal; text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);';
const STYLE_TD = 'background: #FFFFFF; padding: 20px; text-align: left; vertical-align: middle; font-weight: 300; font-size: 18px; text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1); border-right: 1px solid #C1C3D1;';

const STYLE_TD_LAST = STYLE_TD + ' border-right: 0px; display: flex; align-items: center;';

const HTML_TEMPLATE = '' +
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
    '<html xmlns="http://www.w3.org/1999/xhtml">' +
    '   <head>' +
    '       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
    '       <title>Scrapping data</title>' +
    '       <meta name="viewport" content="width=device-width, initial-scale=1.0"/>' +
    '   </head>' +
    '   <body style="background-color: #3e94ec; font-family: helvetica, arial, sans-serif; font-size: 16px; font-weight: 400;">' +
    '       <table style="background: white; border-radius:3px; border-collapse: collapse; height: 320px; margin: auto; max-width: 600px; padding:5px; width: 100%; box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);">' +
    '           {0}' +
    '       </table>' +
    '   </body>' +
    '</html>';

let transporter = mailer.createTransport({
    service: 'gmail',
    auth: {
        user: configs.broadcasterEmail,
        pass: configs.broadcasterPass
    }
});

function compositeHtml(data) {
    console.log('compiling html table....');
    let table = '';
    data.forEach((site) => {
        site.forEach((item) => {
            table += '' +
                '           <tr style="' + STYLE_TR + '">' +
                '               <td style="' + STYLE_TD + '">' +
                '                   ' +
                item.name +
                '               </td>' +
                '               <td style="' + STYLE_TD_LAST + '">' +
                '                   <span>' +
                item.data +
                '                   </span>';

            if (item.icon) {
                table += '<img src="' + item.icon + '" style="max-width: 40px; max-height: 40px;">';
            }
            table += '' +
                '               </td>' +
                '           </tr>';
        })
    });
    return HTML_TEMPLATE.replace('{0}', table);
}

/**
 * Send email.
 *
 * @param {String} subject
 * @param {Array} data
 * @param {String} to comma separated list of addresses
 */
function sendMail(subject, data, to) {
    let htmlString = compositeHtml(data);

    if (AppModel.isModeRunning(CONSTS.DEBUG_MODE)) {
        fs.writeFileSync(CONSTS.DEBUG_FILE_NAME, htmlString, 'utf-8');
        console.log(CONSTS.DEBUG_MODE, 'detected. Result written to', CONSTS.DEBUG_FILE_NAME);
        return;
    }

    let mailOptions = {
        from: '"Pavel Broadcaster" <pavel.broadcaster@gmail.com>',
        to: to,
        subject: subject,
        html: htmlString
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log('ERROR:', error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
}

module.exports = {
    sendMail: sendMail
};
