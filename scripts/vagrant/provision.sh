#!/usr/bin/env bash

sudo apt-get update

echo "Installing curl…"
sudo apt-get install curl -y
echo "Curl installation finished."

echo "Installing git..."
sudo apt-get install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev -y
echo "Git installation finished."

echo "Installing nodejs…"
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
echo "Nodejs installation finished."

echo "Installing npm dependencies..."
cd ../../vagrant/
rm -rf node_modules && npm install
echo "Npm dependencies installation finished."

echo "Installing APT packages...";
echo "Installing virtual buffer dependencies...";
sudo apt-get install pkg-config libjpeg-dev libgif-dev g++ git-all xvfb libgtk2.0-0 dbus-x11 -y;
sudo apt-get install libnss3-dev -y;
sudo apt-get install gtk2-engines-pixbuf xfonts-cyrillic xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable -y;
