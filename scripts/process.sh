#!/usr/bin/env bash

SLEEP_VALUE=$((5 * 60))
GIT_ROOT=$(dirname `which $0`)/../
source ${GIT_ROOT}/scripts/common/config.sh;

cd ${GIT_ROOT}

updateRemote
checkResult=($(checkIfPullNeeded));
echo "Git status ${checkResult}"
if [ "${checkResult}" == "${NEED_TO_PULL}" ]; then
    echo "Do git pull"
    gitPull

    isPackageJSON=($(checkChangedFiles package.json));
    if [ "${isPackageJSON}" ]; then
        echo "Installing npm dependencies..."
        npm install
    fi

    echo "Transpiling files..."
    npm run transpile
fi

echo "Invoking process..."
npm run start

if [ $? != 0 ]; then
    echo "node exit with status $?"
    echo "retrying in ${SLEEP_VALUE} seconds"
    sleep ${SLEEP_VALUE}
    echo "-------------------------------------------------------------------------------------------------------------"
    ./$0
fi
