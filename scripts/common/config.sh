#!/usr/bin/env bash

UP_TO_DATE="Up-to-date";
NEED_TO_PULL="Need-to-pull";
NEED_TO_PUSH="Need-to-push";
DIVERGED="Diverged";

function updateRemote {
    git remote update
    CHANGED_FILES="$(git diff-tree -r --name-only --no-commit-id origin/master master)"
}

function checkIfPullNeeded {
    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @{0})
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @{0} "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
        echo ${UP_TO_DATE};
    elif [ $LOCAL = $BASE ]; then
        echo ${NEED_TO_PULL};
    elif [ $REMOTE = $BASE ]; then
        echo ${NEED_TO_PUSH};
    else
        echo ${DIVERGED};
    fi
}

function gitPull {
    git reset --hard origin/master
}

function checkChangedFiles() {
    echo "$CHANGED_FILES" | grep "$1";
}
