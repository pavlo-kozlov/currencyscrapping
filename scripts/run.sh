#!/usr/bin/env bash

GIT_ROOT=$(dirname `which $0`)/../

cd ${GIT_ROOT}

LOGS_DIR="./logs/"
LOG_FILE_PATTERN="%Y-%m-%d-%H-%M"
NOW=$(date +${LOG_FILE_PATTERN})
LOG_FILE="${LOGS_DIR}${NOW}.txt"

if [ ! -d "$LOGS_DIR" ]
then
    echo "${LOGS_DIR} doesn't exist. Creating now"
    mkdir $LOGS_DIR
    echo "Directory created"
else
    echo "${LOGS_DIR} exists"
fi

echo ${NOW};
touch ${LOG_FILE}

./scripts/process.sh > ${LOG_FILE}
